# EASY

# Return the argument with all its lowercase characters removed.
def destructive_uppercase(str)
  upcase = ('A'..'Z').to_a
  new_string = []
  str.each_char do |char|
    new_string << char if upcase.include?(char)
  end
  new_string.join
end

# Return the middle character of a string. Return the middle two characters if
# the word is of even length, e.g. middle_substring("middle") => "dd",
# middle_substring("mid") => "i"
def middle_substring(str)
  if even_length?(str)
    str[(str.length / 2) - 1] + str[str.length / 2]
  else
    str[str.length / 2]
  end
end

def even_length?(str)
  str.length % 2 == 0
end

# Return the number of vowels in a string.
VOWELS = %w(a e i o u).freeze
def num_vowels(str)
  vowel_count = 0
  str.each_char do |char|
    vowel_count += 1 if VOWELS.include?(char)
  end
  vowel_count
end

# Return the factoral of the argument (num). A number's factorial is the product
# of all whole numbers between 1 and the number itself. Assume the argument will
# be > 0.
def factorial(num)
  factorial = 1
  (1..num).to_a.each do |n|
    factorial *= n
  end
  factorial
end


# MEDIUM

# Write your own version of the join method. separator = "" ensures that the
# default seperator is an empty string.
def my_join(arr, separator = "")
  new_string = ""
  arr.each do |el|
    new_string += el.to_s + separator
  end
  if new_string[-1] == separator
    new_string = new_string[0...-1]
  end
  new_string
end

# Write a method that converts its argument to weirdcase, where every odd
# character is lowercase and every even is uppercase, e.g.
# weirdcase("weirdcase") => "wEiRdCaSe"
def weirdcase(str)
  new_string = ""

  str.chars.each_with_index do |char, idx|
    if idx % 2 == 0
      new_string += char.downcase
    else
      new_string += char.upcase
    end

  end
  new_string
end

# Reverse all words of five more more letters in a string. Return the resulting
# string, e.g., reverse_five("Looks like my luck has reversed") => "skooL like
# my luck has desrever")
def reverse_five(str)
  reversed = []
  words = str.split(' ')
  words.each do |w|
    if w.length >= 5
      reversed << w.reverse
    else
      reversed << w
    end
  end
  reversed.join(' ')
end

# Return an array of integers from 1 to n (inclusive), except for each multiple
# of 3 replace the integer with "fizz", for each multiple of 5 replace the
# integer with "buzz", and for each multiple of both 3 and 5, replace the
# integer with "fizzbuzz".
def fizzbuzz(n)
  new_array = []
  (1..n).to_a.each do |el|
    if (el % 5 == 0) && (el % 3 == 0)
      new_array << "fizzbuzz"
    elsif el % 5 == 0
      new_array << "buzz"
    elsif el % 3 == 0
      new_array << "fizz"
    else
      new_array << el
    end
  end
  new_array
end


# HARD

# Write a method that returns a new array containing all the elements of the
# original array in reverse order.
def my_reverse(arr)
  idx_array = (0..arr.length - 1).to_a.reverse
  new_array = []
  idx_array.each do |idx|
    new_array << arr[idx]
  end
  new_array
end

# Write a method that returns a boolean indicating whether the argument is
# prime.
def prime?(num)
  return false if num <= 1
  (2...num).to_a.each do |n|
    return false if num % n == 0
  end
  true
end

# Write a method that returns a sorted array of the factors of its argument.
def factors(num)
  factors = []
  (1..num).to_a.each do |n|
    factors << n if num % n == 0
  end

  factors.sort
end

# Write a method that returns a sorted array of the prime factors of its argument.
def prime_factors(num)
  prime_fac = []
  factors(num).each do |f|
    prime_fac << f if prime?(f)
  end
  prime_fac
end

# Write a method that returns the number of prime factors of its argument.
def num_prime_factors(num)
  prime_factors(num).count
end


# EXPERT

# Return the one integer in an array that is even or odd while the rest are of
# opposite parity, e.g. oddball([1,2,3]) => 2, oddball([2,4,5,6] => 5)
def oddball(arr)
  even = []
  odd = []
  arr.each do |n|
    n % 2 == 0 ? even << n : odd << n
  end
  even.length > 1 ? odd[0] : even[0]
end
